/*
 * Implement bukkit plugin interface
 */

package xeth.dyearmor

import java.util.EnumSet
import org.bukkit.Color
import org.bukkit.ChatColor
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.LeatherArmorMeta
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.block.Action
import org.bukkit.scheduler.BukkitRunnable
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.TextComponent

internal val LEATHER_ARMOR_TYPES: EnumSet<Material> = EnumSet.of(
    Material.LEATHER_CHESTPLATE,
    Material.LEATHER_LEGGINGS,
    Material.LEATHER_HELMET,
    Material.LEATHER_BOOTS
)


public object Message {

    public val PREFIX = "[Dye]"
    public val COL_MSG = ChatColor.DARK_GREEN
    public val COL_ERROR = ChatColor.RED

    // print generic message to chat
    public fun print(sender: Any?, s: String) {
        val msg = "${COL_MSG}${s}"
        if ( sender is Player ) {
            sender.sendMessage(msg)
        }
        else {
            (sender as CommandSender).sendMessage(msg)
        }
    }

    // print error message to chat
    public fun error(sender: Any?, s: String) {
        val msg = "${COL_ERROR}${s}"
        if ( sender is Player ) {
            sender.sendMessage(msg)
        }
        else {
            (sender as CommandSender).sendMessage(msg)
        }
    }
}

public class DyeArmorCommand(val plugin: DyeArmorPlugin) : CommandExecutor, TabCompleter {

    val SUBCOMMANDS = listOf("help", "reload")

    override fun onCommand(sender: CommandSender, cmd: Command, commandLabel: String, args: Array<String>): Boolean {
        
        val player = if ( sender is Player ) sender else null

        // no args, open flags gui
        if ( args.size == 0 ) {
            printHelp(sender)
            return true
        }

        // parse subcommand
        val arg = args[0].lowercase()
        when ( arg ) {
            "help" -> printHelp(sender)
            "reload" -> reload(sender)
            else -> {
                if ( player === null ) {
                    Message.print(sender, "/dyearmor [r] [g] [b]${ChatColor.WHITE}: dye leather armor red, green, blue (0-255)")
                    return true
                }

                if ( args.size >= 3 ) {
                    try {
                        val r = args[0].toInt().coerceIn(0, 255)
                        val g = args[1].toInt().coerceIn(0, 255)
                        val b = args[2].toInt().coerceIn(0, 255)
                        dyeArmor(player, r, g, b)
                        Message.print(sender, "Dyed armor: red: ${r}, green: ${g}, blue: ${b}")
                    }
                    catch ( err: Exception ) {
                        Message.print(sender, "Invalid number, red, green, blue must be integer 0-255")
                    }
                }
                else {
                    printHelp(sender)
                }
            }
        }

        return true
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<String>): List<String> {
        return SUBCOMMANDS
    }

    private fun printHelp(sender: CommandSender?) {
        Message.print(sender, "/dyearmor [r] [g] [b]${ChatColor.WHITE}: dye leather armor red, green, blue (0-255)")
        Message.print(sender, "Example: /dyearmor 250 0 0${ChatColor.WHITE}: will dye leather armor red")
        return
    }

    private fun reload(sender: CommandSender?) {
        val player = if ( sender is Player ) sender else null
        if ( player === null || player.isOp() ) {
            // plugin.reload()
            Message.print(sender, "[dyearmor] reloaded")
        }
        else {
            Message.error(sender, "[dyearmor] Only operators can reload")
        }
    }

    private fun dyeArmor(player: Player, r: Int, g: Int, b: Int) {
        val color = Color.fromRGB(r, g, b)
        val inventory = player.inventory
        val invSize = inventory.getSize()
        for ( i in 0 until invSize ) {
            val item = inventory.getItem(i)
            if ( item !== null && LEATHER_ARMOR_TYPES.contains(item.type) ) {
                val meta = item.getItemMeta()
                if ( meta !== null ) {
                    (meta as LeatherArmorMeta).setColor(color)
                    item.setItemMeta(meta)
                }
            }
            player.updateInventory()
        }
    }
}

public class DyeArmorPlugin : JavaPlugin() {

    override fun onEnable() {
        // measure load time
        val timeStart = System.currentTimeMillis()
        val logger = this.getLogger()

        // register commands
        this.getCommand("dyearmor")?.setExecutor(DyeArmorCommand(this))

        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    override fun onDisable() {
        logger.info("wtf i hate xeth now")
    }
}
